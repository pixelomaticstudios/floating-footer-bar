<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://www.wphats.com/
 * @since      1.0.0
 *
 * @package    Floating_Footer_Bar
 * @subpackage Floating_Footer_Bar/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Floating_Footer_Bar
 * @subpackage Floating_Footer_Bar/public
 * @author     Pixelsigns Team <mehedi.doha@gmail.com>
 */
class Floating_Footer_Bar_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Floating_Footer_Bar_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Floating_Footer_Bar_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		$enable = get_option('pixelsigns_enable_floating_footer_bar');
		if( 'yes' === $enable ) {
			wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/floating-footer-bar-public.css', array(), $this->version, 'all' );

			$background_color = get_option( 'pixelsigns_floating_footer_bar_background_color' );
			$background_color = !empty($background_color) ? $background_color : '';

			$secondary_background_color = get_option( 'pixelsigns_floating_footer_bar_secondary_background_color' );
			$secondary_background_color = !empty($secondary_background_color) ? $secondary_background_color : '';

			$text_color = get_option( 'pixelsigns_floating_footer_bar_text_color' );
			$text_color = !empty($text_color) ? $text_color : '';

			$anchor_color = get_option( 'pixelsigns_floating_footer_bar_anchor_color' );
			$anchor_color = !empty($anchor_color) ? $anchor_color : '';

			$custom_css = "
            .pix_foobar_section{
                    background: {$background_color};
            }
            .pix_foobar_wrapper, .pix_foobar_wrapper p{
            	color: {$text_color};
            }

            .pix_foobar_wrapper a {
            	color: {$anchor_color};
            }
            .pix_foobar_section.template_5, .pix_foobar_section.template_3 {
            	background: none;
            }
            .pix_foobar_section.template_3::after, .pix_foobar_section.template_3::before, .pix_foobar_section.template_5::after, .pix_foobar_section.template_5::before {
            	background: {$background_color};
            }
            .pix_foobar_section.template_3::after {
            	background: {$secondary_background_color};
            }
            ";
            wp_add_inline_style( $this->plugin_name, $custom_css );
		}

		

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Floating_Footer_Bar_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Floating_Footer_Bar_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/floating-footer-bar-public.js', array( 'jquery' ), $this->version, false );

	}

	/**
	 * Add floating footer content
	 */
	public function pixelsigns_floating_footer_bar_html() {

		$enable = get_option('pixelsigns_enable_floating_footer_bar');
		$content = get_option( 'pixelsigns_floating_footer_bar_content' );
		$content_left = get_option('pixelsigns_floating_footer_bar_content_left');
		$image = get_option('pixelsigns_floating_footer_bar_image');
		$image_position = get_option('pixelsigns_floating_footer_bar_image_position');
		$template = ( !null == get_option('pixelsigns_floating_footer_bar_template') ) ? get_option('pixelsigns_floating_footer_bar_template') : '';
		$template_class = ( 'template_6' === $template ) ? 'template_4 template_6' : $template;
		$template_class = ( 'template_6' === $template ) && ( 'right' === $image_position ) ? $template_class . ' img_flip' : $template_class;


		if( is_front_page() && (isset($_GET['page'])) && (isset($_GET['id'])) ) { ?>
			<div class="pix_foobar_section area<?php echo ' ' . $template_class; ?>">
		        <div class="cus_container">
		            <div class="pix_foobar_wrapper">
					<?php if( 'template_6' === $template ) {
		            		?>
		            		<?php if( !empty($image) ) { ?>
		            		<div class="pix_foobar_img">
			                    <img src="<?php esc_attr_e($image); ?>" alt="">
			                </div>
						<?php } ?>
			                <div class="pix_foobar_inner">
			                    <?php echo $content; ?>
			                </div>
		            		<?php
		            	} elseif( 'template_5' === $template && !empty( $content_left ) ) { ?>
							<div class="pix_foobar_inner">
								<?php echo $content_left; ?>
							</div>
							<div class="pix_foobar_inner right">
			                    <?php echo $content; ?>
			                </div>
						<?php } else { echo $content; } ?>
		            </div>
		            <!-- /.pix_foobar_wrapper -->
		        </div>
		        <!-- /.container -->
		    </div>
			    <!-- /.pix_foobar_section -->
			    <?php
		} else {

		$enable_all_pages = get_option('pixelsigns_floating_footer_bar_enable_page');
		
		global $post;
		$page_enable = get_post_meta( $post->ID, 'pixelsigns_floating_footer_bar_page_enable', true );
		

		if( ('yes' === $enable) ) {

			if( ('yes' === $enable_all_pages) && ('yes' === $page_enable) ) {
			?>

			<div class="pix_foobar_section area<?php echo ' ' . $template_class; ?>">
		        <div class="cus_container">
		            <div class="pix_foobar_wrapper">
		            	<?php if( 'template_6' === $template ) {
		            		?>
		            		<?php if( !empty($image) ) { ?>
		            		<div class="pix_foobar_img">
			                    <img src="<?php esc_attr_e($image); ?>" alt="">
			                </div>
						<?php } ?>
			                <div class="pix_foobar_inner">
			                    <?php echo $content; ?>
			                </div>
		            		<?php
		            	} elseif( 'template_5' === $template && !empty( $content_left ) ) { ?>
							<div class="pix_foobar_inner">
								<?php echo $content_left; ?>
							</div>
							<div class="pix_foobar_inner right">
			                    <?php echo $content; ?>
			                </div>
						<?php } else { echo $content; } ?>
		            </div>
		            <!-- /.pix_foobar_wrapper -->
		        </div>
		        <!-- /.container -->
		    </div>
			    <!-- /.pix_foobar_section -->

			<?php
			} elseif( ('yes' !== $enable_all_pages) ) {
				if( ('yes' === $page_enable) ) {
				?>
				<div class="pix_foobar_section area<?php echo ' ' . $template_class; ?>">
		        <div class="cus_container">
		            <div class="pix_foobar_wrapper">
					<?php if( 'template_6' === $template ) {
		            		?>
		            		<?php if( !empty($image) ) { ?>
		            		<div class="pix_foobar_img">
			                    <img src="<?php esc_attr_e($image); ?>" alt="">
			                </div>
						<?php } ?>
			                <div class="pix_foobar_inner">
			                    <?php echo $content; ?>
			                </div>
		            		<?php
		            	} elseif( 'template_5' === $template && !empty( $content_left ) ) { ?>
							<div class="pix_foobar_inner">
								<?php echo $content_left; ?>
							</div>
							<div class="pix_foobar_inner right">
			                    <?php echo $content; ?>
			                </div>
						<?php } else { echo $content; } ?>
		            </div>
		            <!-- /.pix_foobar_wrapper -->
		        </div>
		        <!-- /.container -->
		    </div>
			    <!-- /.pix_foobar_section -->
				<?php
				}
			}
		}

		
		}

	}

}
