<?php

/**
 * Fired during plugin activation
 *
 * @link       http://www.wphats.com/
 * @since      1.0.0
 *
 * @package    Floating_Footer_Bar
 * @subpackage Floating_Footer_Bar/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Floating_Footer_Bar
 * @subpackage Floating_Footer_Bar/includes
 * @author     Pixelsigns Team <mehedi.doha@gmail.com>
 */
class Floating_Footer_Bar_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
