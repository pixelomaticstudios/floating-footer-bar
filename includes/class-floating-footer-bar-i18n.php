<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       http://www.wphats.com/
 * @since      1.0.0
 *
 * @package    Floating_Footer_Bar
 * @subpackage Floating_Footer_Bar/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Floating_Footer_Bar
 * @subpackage Floating_Footer_Bar/includes
 * @author     Pixelsigns Team <mehedi.doha@gmail.com>
 */
class Floating_Footer_Bar_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'floating-footer-bar',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
