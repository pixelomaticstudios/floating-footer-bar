<?php
/**
 * The admin menu page functionality of the plugin.
 *
 * @link       http://www.wphats.com/
 * @since      1.0.0
 *
 * @package    Floating_Footer_Bar
 * @subpackage Floating_Footer_Bar/admin/classes
 */

class Floating_Footer_Bar_Admin_Settings
{
	
	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the Floating Footer Bar for the admin area
	 *
	 * @since 		1.0.0
	 */
	public function admin_menu_page() {

		/**
		 * add Floating Footer Bar page
		 */

		add_menu_page(
            esc_html__('Floating Footer Bar', 'floating-footer-bar'),
            esc_html__('Floating Footer Bar', 'floating-footer-bar'),
            "manage_options",
            "pixelsigns-floating-footer-bar",
            array($this, "pixelsigns_floating_footer_page_content"),
            "dashicons-tickets-alt",
            100
        );

	}

	/**
	 * Register the admin page settings
	 *
	 * @since 1.0.0
	 */

	public function pixelsigns_floating_footer_page_content() {


		$enable_pages = get_option('pixelsigns_floating_footer_bar_enable_page');
		$pages = get_pages(); 
		  foreach ( $pages as $page ) {
		   update_post_meta( $page->ID, 'pixelsigns_floating_footer_bar_page_enable', 'yes' );
		  }

		?>
            <div id="pixelsigns-ffb-settings-page-wrap" class="wrap">
	            <div id="icon-options-general" class="icon32"></div>
	            <h1><?php esc_html_e( 'Floating Footer Bar', 'floating-footer-bar' ); ?></h1>
	            <?php settings_errors(); ?>
	            <form method="post" action="options.php">
	                <?php
	               
	                    //add_settings_section callback is displayed here. For every new section we need to call settings_fields.
	                    settings_fields("pixelsigns-floating-footer-bar-settings-group");
	                   
	                    // all the add_settings_field callbacks is displayed here
	                    do_settings_sections("pixelsigns-floating-footer-bar-options");
	               
	                    // Add the submit button to serialize the options
	                    submit_button();
	                   
	                ?>         
	            </form>
        	</div>
        <?php

	}

	/**
	 * Display the admin page settings fields
	 *
	 * @since 1.0.0
	 */

	public function pixelsigns_floating_footer_bar_settings() {
		//section name, display name, callback to print description of section, page to which section is attached.
        add_settings_section( "pixelsigns-floating-footer-bar-settings-group", esc_html__('Settings','loating-footer-bar'), "", "pixelsigns-floating-footer-bar-options" );

        //setting name, display name, callback to print form element, page in which field is displayed, section to which it belongs.
        //last field section is optional.
        add_settings_field( "pixelsigns_enable_floating_footer_bar", esc_html__("Enable Floating Footer Bar","floating-footer-bar"), array( $this, 'display_enable_floating_footer_bar_content' ), "pixelsigns-floating-footer-bar-options", "pixelsigns-floating-footer-bar-settings-group" );
        add_settings_field( "pixelsigns_floating_footer_bar_enable_page", esc_html__("Enable Floating Footer Bar for all pages","floating-footer-bar"), array( $this, 'display_floating_footer_bar_enable_pages' ), "pixelsigns-floating-footer-bar-options", "pixelsigns-floating-footer-bar-settings-group" );
        add_settings_field( "pixelsigns_floating_footer_bar_template", esc_html__("Template","floating-footer-bar"), array( $this, 'display_floating_footer_bar_template' ), "pixelsigns-floating-footer-bar-options", "pixelsigns-floating-footer-bar-settings-group" );
        add_settings_field( "pixelsigns_floating_footer_bar_content", esc_html__("Content","floating-footer-bar"), array( $this, 'display_floating_footer_bar_content' ), "pixelsigns-floating-footer-bar-options", "pixelsigns-floating-footer-bar-settings-group" );
        add_settings_field( "pixelsigns_floating_footer_bar_content_left", esc_html__("Left Align Content","floating-footer-bar"), array( $this, 'display_floating_footer_bar_content_left' ), "pixelsigns-floating-footer-bar-options", "pixelsigns-floating-footer-bar-settings-group" );
        add_settings_field( "pixelsigns_floating_footer_bar_background_color", esc_html__("Background Color","floating-footer-bar"), array( $this, 'display_floating_footer_bar_background_color' ), "pixelsigns-floating-footer-bar-options", "pixelsigns-floating-footer-bar-settings-group" );
        add_settings_field( "pixelsigns_floating_footer_bar_secondary_background_color", esc_html__("Secondary Background Color","floating-footer-bar"), array( $this, 'display_floating_footer_bar_secondary_background_color' ), "pixelsigns-floating-footer-bar-options", "pixelsigns-floating-footer-bar-settings-group" );
        add_settings_field( "pixelsigns_floating_footer_bar_text_color", esc_html__("Text Color","floating-footer-bar"), array( $this, 'display_floating_footer_bar_text_color' ), "pixelsigns-floating-footer-bar-options", "pixelsigns-floating-footer-bar-settings-group" );
        add_settings_field( "pixelsigns_floating_footer_bar_anchor_color", esc_html__("Anchor Color","floating-footer-bar"), array( $this, 'display_floating_footer_bar_anchor_color' ), "pixelsigns-floating-footer-bar-options", "pixelsigns-floating-footer-bar-settings-group" );
        add_settings_field( "pixelsigns_floating_footer_bar_image", esc_html__("Upload Image","floating-footer-bar"), array( $this, 'display_floating_footer_bar_image' ), "pixelsigns-floating-footer-bar-options", "pixelsigns-floating-footer-bar-settings-group" );
        add_settings_field( "pixelsigns_floating_footer_bar_image_position", esc_html__("Image position","floating-footer-bar"), array( $this, 'display_floating_footer_bar_image_position' ), "pixelsigns-floating-footer-bar-options", "pixelsigns-floating-footer-bar-settings-group" );
        add_settings_field( "pixelsigns_floating_footer_bar_preview", esc_html__("Preview","floating-footer-bar"), array( $this, 'display_floating_footer_bar_preview' ), "pixelsigns-floating-footer-bar-options", "pixelsigns-floating-footer-bar-settings-group" );

        //section name, form element name, callback for sanitization
        register_setting( "pixelsigns-floating-footer-bar-settings-group", "pixelsigns_enable_floating_footer_bar" 						);
        register_setting( "pixelsigns-floating-footer-bar-settings-group", "pixelsigns_floating_footer_bar_enable_page" 				);
        register_setting( "pixelsigns-floating-footer-bar-settings-group", "pixelsigns_floating_footer_bar_background_color" 			);
        register_setting( "pixelsigns-floating-footer-bar-settings-group", "pixelsigns_floating_footer_bar_secondary_background_color" 	);
        register_setting( "pixelsigns-floating-footer-bar-settings-group", "pixelsigns_floating_footer_bar_text_color" 					);
        register_setting( "pixelsigns-floating-footer-bar-settings-group", "pixelsigns_floating_footer_bar_anchor_color" 				);
        register_setting( "pixelsigns-floating-footer-bar-settings-group", "pixelsigns_floating_footer_bar_content" 					);
        register_setting( "pixelsigns-floating-footer-bar-settings-group", "pixelsigns_floating_footer_bar_content_left" 				);
        register_setting( "pixelsigns-floating-footer-bar-settings-group", "pixelsigns_floating_footer_bar_template" 					);
        register_setting( "pixelsigns-floating-footer-bar-settings-group", "pixelsigns_floating_footer_bar_image" 						);
        register_setting( "pixelsigns-floating-footer-bar-settings-group", "pixelsigns_floating_footer_bar_image_position" 				);
        register_setting( "pixelsigns-floating-footer-bar-settings-group", "pixelsigns_floating_footer_bar_preview" 					);
	}

	/**
	 * Display Enable/disable settings field
	 */
	public function display_enable_floating_footer_bar_content() {

		$enable = get_option('pixelsigns_enable_floating_footer_bar');
		?>
		
		<label for="pixelsigns_enable_floating_footer_bar" class="switch">
			<input type="checkbox" name="pixelsigns_enable_floating_footer_bar" id="pixelsigns_enable_floating_footer_bar" value="yes"<?php if( 'yes' === $enable ) { echo ' checked="checked"'; } ?>/>
            <span class="slider round"></span>
		</label>

        <?php

	}

	/**
	 * Enable for all pages
	 */
	public function display_floating_footer_bar_enable_pages() {

		$enable_pages = get_option('pixelsigns_floating_footer_bar_enable_page');	?>
		
		<label for="pixelsigns_floating_footer_bar_enable_page" class="switch">
			<input type="checkbox" name="pixelsigns_floating_footer_bar_enable_page" id="pixelsigns_floating_footer_bar_enable_page" value="yes"<?php if( 'yes' === $enable_pages ) { echo ' checked="checked"'; } ?>/>

            <span class="slider round"></span>
		</label><br/>
		<p class="pix-control-description"><?php esc_html_e('If chekced then enable floating footer bar for all the pages in the site. You can alter the settings in single page too using metabox.','floating-footer-bar'); ?></p>

        <?php

	}

	/**
	 * Display footer content
	 */
	public function display_floating_footer_bar_content() {

		$pixelsigns_floating_footer_bar_content = get_option( 'pixelsigns_floating_footer_bar_content' );
    	echo wp_editor( $pixelsigns_floating_footer_bar_content, 'pixelsignsfloatingfootercontent', array('textarea_name' => 'pixelsigns_floating_footer_bar_content')  );

	}

	/**
	 * Display footer content
	 */
	public function display_floating_footer_bar_content_left() {

		$pixelsigns_floating_footer_bar_content_left = get_option( 'pixelsigns_floating_footer_bar_content_left' );
    	echo wp_editor( $pixelsigns_floating_footer_bar_content_left, 'pixelsignsfloatingfootercontentleft', array('textarea_name' => 'pixelsigns_floating_footer_bar_content_left')  );

	}

	/**
	 * Display template
	 */
	public function display_floating_footer_bar_template() {
		?>
		<div class="pixelsigns-ffb-template-selector">
			<?php for($i=1; $i<=6; $i++): ?>
				<div class="pixelsigns-ffb-template-item">
					<input type="radio" name="pixelsigns_floating_footer_bar_template" id="pixelsigns_floating_footer_bar_template_<?php echo $i; ?>" value="template_<?php echo $i; ?>" <?php if(get_option('pixelsigns_floating_footer_bar_template') == "template_".$i) { echo 'checked'; } ?>>
					<label for="pixelsigns_floating_footer_bar_template_<?php echo $i;?>" class="pixelsigns-ffb-template">
						<img src="<?php echo esc_url(PFFB_ASSETS) ; ?>img/Temp_<?php echo $i; ?>.png" alt="Default Template <?php echo $i; ?>" width="200" height="120">
					</label>
				</div>
			<?php endfor; ?>
		</div>
		<?php
	}

	/**
	 * Background Color
	 */
	public function display_floating_footer_bar_background_color() {

		$background_color = get_option( 'pixelsigns_floating_footer_bar_background_color' );
		$background_color = !empty($background_color) ? $background_color : '';
    	echo '<input type="text" name="pixelsigns_floating_footer_bar_background_color" value="' . $background_color . '" class="pixelsigns-floating-footer-bar-colorpicker" >';

	}

	/**
	 * Secondary Background Color
	 */
	public function display_floating_footer_bar_secondary_background_color() {

		$secondary_background_color = get_option( 'pixelsigns_floating_footer_bar_secondary_background_color' );
		$secondary_background_color = !empty($secondary_background_color) ? $secondary_background_color : '';
    	echo '<input type="text" name="pixelsigns_floating_footer_bar_secondary_background_color" value="' . $secondary_background_color . '" class="pixelsigns-floating-footer-bar-colorpicker" >';

	}

	/**
	 * Text Color
	 */
	public function display_floating_footer_bar_text_color() {

		$text_color = get_option( 'pixelsigns_floating_footer_bar_text_color' );
		$text_color = !empty($text_color) ? $text_color : '';
    	echo '<input type="text" name="pixelsigns_floating_footer_bar_text_color" value="' . $text_color . '" class="pixelsigns-floating-footer-bar-colorpicker" >';

	}

	/**
	 * Anchor Color
	 */
	public function display_floating_footer_bar_anchor_color() {

		$anchor_color = get_option( 'pixelsigns_floating_footer_bar_anchor_color' );
		$anchor_color = !empty($anchor_color) ? $anchor_color : '';
    	echo '<input type="text" name="pixelsigns_floating_footer_bar_anchor_color" value="' . $anchor_color . '" class="pixelsigns-floating-footer-bar-colorpicker" >';

	}

	/**
	 * template 6 image
	 */
	public function display_floating_footer_bar_image(){
		$image = get_option('pixelsigns_floating_footer_bar_image');
		?>
		<div>
            <img class="upload_preview" src="<?php echo esc_attr($image)?>" />
        </div>
    <input type="hidden" class="upload_field" name="pixelsigns_floating_footer_bar_image" id="logotype" value="<?php echo esc_attr($image); ?>" />
    <input type="button" class="button upload_button" value="Upload" />
    <input type="button" class="button upload_clear" value="Remove" />
		<?php
	}

	/**
	 * Image Position
	 */
	public function display_floating_footer_bar_image_position() {
		$image_position = get_option('pixelsigns_floating_footer_bar_image_position');
		?>
		<input type="radio" name="pixelsigns_floating_footer_bar_image_position" id="pixelsigns_floating_footer_bar_image_position_left" value="left" <?php if($image_position == "left") { echo 'checked'; } ?>>
		<label for="pixelsigns_floating_footer_bar_image_position_left"><?php esc_html_e('Left','floating-footer-bar'); ?></label>
		<input type="radio" name="pixelsigns_floating_footer_bar_image_position" id="pixelsigns_floating_footer_bar_image_position_right" value="right" <?php if($image_position == "right") { echo 'checked'; } ?>>
		<label for="pixelsigns_floating_footer_bar_image_position_right"><?php esc_html_e('Right','floating-footer-bar'); ?></label>
		<?php
	}

	/**
	 * Preview Button
	 */
	public function display_floating_footer_bar_preview() {
		echo '<a class="button" href="'.get_site_url().'/?page=prview&id='.get_option('pixelsigns_floating_footer_bar_template').'" target="_blank">'.esc_html__('Preview','floating-footer-bar').'</a></br>';
		echo '<p class="pix-control-description">'. esc_html__("Show preview after save the changes",'floating-footer-bar') .'</p>';
	}

}