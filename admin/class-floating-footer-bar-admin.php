<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://www.wphats.com/
 * @since      1.0.0
 *
 * @package    Floating_Footer_Bar
 * @subpackage Floating_Footer_Bar/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Floating_Footer_Bar
 * @subpackage Floating_Footer_Bar/admin
 * @author     Pixelsigns Team <mehedi.doha@gmail.com>
 */
class Floating_Footer_Bar_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		// enable the default wp color picker stylesheet
		wp_enqueue_style( 'wp-color-picker' );

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/floating-footer-bar-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since 		1.0.0
	 */
	public function enqueue_scripts() {

		wp_enqueue_media();

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/floating-footer-bar-admin.js', array( 'jquery', 'wp-color-picker' ), $this->version, true );

	}

	/**
	 * Add meta boxes
	 */
	public function pixelsigns_floating_footer_bar_metaboxes() {

		add_meta_box( 'pixelsigns-floating-footer-bar-metabox', __( 'Floating Footer Bar Settings', 'floating-footer-bar' ), array( $this, 'pixelsigns_floating_footer_bar_metabox_content' ), 'page', 'side', 'high' );

	}

	/**
	 * display meta fields for notes meta
	 *
	 * @return void
	 */

	public function pixelsigns_floating_footer_bar_metabox_content( $post ) {

		// Use nonce for verification
		wp_nonce_field( 'pixelsigns_floating_footer_bar_meta_nonce', 'pixelsigns_floating_footer_bar_meta_nonce' );

		$post_id	= $post->ID;
		$page_enable = get_post_meta( $post_id, 'pixelsigns_floating_footer_bar_page_enable', true );
		
		?>
		<label for="pixelsigns_floating_footer_bar_page_enable">
			<input type="checkbox" name="pixelsigns_floating_footer_bar_page_enable" id="pixelsigns_floating_footer_bar_page_enable" value="yes"<?php if( 'yes' === $page_enable ) { echo ' checked="checked"'; } ?> /> <?php esc_html_e('Enable Floating Footer','floating-footer-bar'); ?>
		</label>
		
		<?php
	}


	/**
	 * save post metadata
	 *
	 * @return void
	 */

	public function pixelsigns_floating_footer_bar_save_metabox( $post_id ) {

		// make sure we aren't using autosave
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
			return $post_id;

		// do our nonce check. ALWAYS A NONCE CHECK
		if ( ! isset( $_POST['pixelsigns_floating_footer_bar_meta_nonce'] ) || ! wp_verify_nonce( $_POST['pixelsigns_floating_footer_bar_meta_nonce'], 'pixelsigns_floating_footer_bar_meta_nonce' ) )
			return $post_id;

		
		$page_enable = ( isset($_POST['pixelsigns_floating_footer_bar_page_enable']) && 'yes' === $_POST['pixelsigns_floating_footer_bar_page_enable']) ? $_POST['pixelsigns_floating_footer_bar_page_enable'] : 'no';
		update_post_meta( $post_id, 'pixelsigns_floating_footer_bar_page_enable', $page_enable );

	}


}
