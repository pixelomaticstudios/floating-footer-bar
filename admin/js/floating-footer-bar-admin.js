function renderMediaUploader() {
	'use strict';

	var file_frame, image_data;

	/**
     * If an instance of file_frame already exists, then we can open it
     * rather than creating a new instance.
     */
	if ( undefined !== file_frame ) {

		file_frame.open();
		return;

	}


	file_frame = wp.media.frames.file_frame = wp.media({
		frame:    'post',
		state:    'insert',
		multiple: false
	});


	file_frame.on( 'insert', function() {

		// Read the JSON data returned from the Media Uploader
		var json = file_frame.state().get( 'selection' ).first().toJSON();

		// First, make sure that we have the URL of an image to display
		if ( 0 > jQuery.trim( json.url.length ) ) {
			return;
		}

		jQuery(".upload_preview").attr( 'src', json.url )
			.attr( 'alt', json.caption )
			.attr( 'title', json.title );
		jQuery(".upload_field").val(json.url);

	});

	// Now display the actual file_frame
	file_frame.open();

}
(function( $ ) {
	'use strict';

	// enable color picker     
	$( '.pixelsigns-floating-footer-bar-colorpicker' ).wpColorPicker();

	// upload media
	$( '.upload_button' ).on( 'click', function( evt ) {

		// Stop the anchor's default behavior
		evt.preventDefault();

		// Display the media uploader
		renderMediaUploader();

	});

	// remove image
	$(".upload_clear").on("click", function(evt){

		// Stop the anchor's default behavior
		evt.preventDefault();
		jQuery(".upload_preview").attr( 'src', '' )
			.attr( 'alt', '' )
			.attr( 'title', '' );
		jQuery(".upload_field").val();

	});


	var template = $("input[name='pixelsigns_floating_footer_bar_template']:checked").val();
	if( 'template_6' !== template ) {
		$("#logotype").parents('tr').hide();
		$("input[name='pixelsigns_floating_footer_bar_image_position']").parents('tr').hide();
	}
	if( 'template_5' !== template ) {
		$('#wp-pixelsignsfloatingfootercontentleft-wrap').parents('tr').hide();
	}

	$("input[name='pixelsigns_floating_footer_bar_template']").on("change",function(){
		var value = $(this).val();
		if( 'template_6' !== value ) {
			$("#logotype").parents('tr').hide();
			$("input[name='pixelsigns_floating_footer_bar_image_position']").parents('tr').hide();
		} else {
			$("#logotype").parents('tr').show();
			$("input[name='pixelsigns_floating_footer_bar_image_position']").parents('tr').show();
		}

		if( 'template_5' === value ) {
			$('#wp-pixelsignsfloatingfootercontentleft-wrap').parents('tr').show();
		} else {
			$('#wp-pixelsignsfloatingfootercontentleft-wrap').parents('tr').hide();
		}
	});



















})( jQuery );
