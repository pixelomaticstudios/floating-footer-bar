<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://www.wphats.com/
 * @since             1.0.0
 * @package           Floating_Footer_Bar
 *
 * @wordpress-plugin
 * Plugin Name:       Floating Footer Bar
 * Plugin URI:        https://pixelsigns.art/
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            Pixelsigns Team
 * Author URI:        http://www.wphats.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       floating-footer-bar
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'FLOATING_FOOTER_BAR_VERSION', '1.0.0' );
define( 'PFFB_ASSETS', plugins_url( '/assets/', __FILE__ ) );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-floating-footer-bar-activator.php
 */
function activate_floating_footer_bar() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-floating-footer-bar-activator.php';
	Floating_Footer_Bar_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-floating-footer-bar-deactivator.php
 */
function deactivate_floating_footer_bar() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-floating-footer-bar-deactivator.php';
	Floating_Footer_Bar_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_floating_footer_bar' );
register_deactivation_hook( __FILE__, 'deactivate_floating_footer_bar' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-floating-footer-bar.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_floating_footer_bar() {

	$plugin = new Floating_Footer_Bar();
	$plugin->run();

}
run_floating_footer_bar();
